const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssPresetEnv = require("postcss-preset-env");

const isDevelopment = process.env.NODE_ENV === "development";

module.exports = {
   entry: "./src/js/index.js",
   output: {
      path: path.resolve(__dirname, "dist"),
      filename: "bundle.js",
   },
   devServer: {
      contentBase: path.resolve(__dirname, "assets"),
   },
   plugins: [
      new HtmlWebpackPlugin({
         filename: "index.html",
         template: "./src/index.html",
         minify: "auto",
      }),
      new MiniCssExtractPlugin({
         filename: isDevelopment ? "[name].css" : "[name].[hash].css",
         chunkFilename: isDevelopment ? "[id].css" : "[id].[hash].css",
      }),
   ],
   resolve: {
      alias: {
         "@controllers": path.resolve(__dirname, "src/js/controllers/"),
         "@models": path.resolve(__dirname, "src/js/models/"),
         "@views": path.resolve(__dirname, "src/js/views/"),
         "@router": path.resolve(__dirname, "src/js/router/"),
         "@routes": path.resolve(__dirname, "src/js/routes/"),
         "@types": path.resolve(__dirname, "src/js/types/"),
         "@components": path.resolve(__dirname, "src/js/views/components/"),
         "@layout": path.resolve(__dirname, "src/js/views/layout/"),
         "@sass": path.resolve(__dirname, "src/sass/"),
         "@localization": path.resolve(__dirname, "src/localization/"),
      },
      extensions: [".js", ".scss"],
   },
   module: {
      rules: [
         {
            test: /\.html$/i,
            loader: "html-loader",
         },
         {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
               loader: "babel-loader",
               options: {
                  presets: ["@babel/preset-env"],
               },
            },
         },
         {
            test: /\.s[a|c]ss$/i,
            exclude: /node_modules/,
            use: [
               isDevelopment
                  ? {
                       loader: "style-loader",
                       options: { injectType: "styleTag" },
                    }
                  : MiniCssExtractPlugin.loader,
               {
                  loader: "css-loader",
                  options: {
                     importLoaders: 2,
                     modules: { localIdentName: "[path][name]__[local]" },
                     sourceMap: isDevelopment,
                  },
               },
               {
                  loader: "postcss-loader",
                  options: {
                     ident: "postcss",
                     plugins: () => [postcssPresetEnv()],
                  },
               },
               {
                  loader: "sass-loader",
                  options: { sourceMap: isDevelopment },
               },
            ],
         },
         {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [
               "file-loader",
               {
                  loader: "image-webpack-loader",
                  options: {
                     bypassOnDebug: true,
                     disable: true,
                     mozjpeg: {
                        progressive: true,
                        quality: 65,
                     },
                     webp: {
                        quality: 65,
                     },
                     outputPath: "assets",
                  },
               },
            ],
         },
      ],
   },
};
