import { routes } from "@routes/mainRoutes";

class Router {
   constructor() {
      this.currentRoute = routes[0]; // About
   }

   changeRoute({ newRoute }) {
      this.currentRoute = newRoute;
   }
}

export default Router;
