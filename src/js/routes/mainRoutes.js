const ABOUT = "About";
const SETTINGS = "Settings";
const OPTION1 = "Option1";
const OPTION2 = "Option2";
const OPTION3 = "Option3";

export const routes = [ABOUT, SETTINGS, OPTION1, OPTION2, OPTION3];
