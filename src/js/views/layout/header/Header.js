import { getTabNavigationMarkup } from "@components/TabNavigation/TabNavigation";
import { getStandardButtonMarkup } from "@components/buttons/StandardButton/StandardButton";
import { getFileuploadButtonMarkup } from "@components/buttons/FileUploadButton/FileUploadButton";
import { getUserInfoMarkup } from "@components/UserInfo/UserInfo";
import { getStarsMarkup } from "@components/Stars/Stars";
import { getFollowersMarkup } from "@components/Followers/Followers";

import { LOGOUT } from "@localization/labels/labels";

import headerStyles from "./_header";
import standardButtonStyles from "@components/buttons/StandardButton/_standardButton";

export const getHeaderMarkup = ({
   firstName,
   lastName,
   phone,
   address,
   currentRating,
   reviewers,
   followers,
   currentRoute,
   isInMobilePhoneMode,
}) => {
   const markup = `
    <header class=${headerStyles.header}>
    <div class=${headerStyles["header__inner-column"]}>
        <div class=${headerStyles["header__inner-row"]}>
            <div class=${headerStyles["button-logout-container"]}>
                ${getStandardButtonMarkup({
                   label: LOGOUT,
                   buttonClass: standardButtonStyles["button--logout"],
                })}
            </div>
        </div>

        <div class=${headerStyles["header__inner-row"]}>
            ${getFileuploadButtonMarkup()}
        </div>

        <div class=${headerStyles["header__inner-row"]}>
            <div class=${headerStyles["header__inner-container"]}>
                <div class=${headerStyles["header__profilephoto-container"]}>
                </div>

                <div class=${headerStyles["header__userinfo-container"]}>
                    ${getUserInfoMarkup({
                       firstName,
                       lastName,
                       address,
                       phone,
                       isInMobilePhoneMode,
                    })}
                </div>

                <div class=${headerStyles["header__usersrating-container"]} usersrating-container">
                    ${getStarsMarkup({ currentRating, reviewers })}
                </div>
            </div>
        </div>
    </div>
    <div class=${headerStyles["navigation-row"]}>
        ${getTabNavigationMarkup({ currentRoute })}
        ${getFollowersMarkup({ followers })}
    </div>
</header>
    `;

   return markup;
};
