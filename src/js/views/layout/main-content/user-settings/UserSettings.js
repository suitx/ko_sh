import styles from "./_user-settings";

export const getUserSettingsMarkup = () => {
   const markup = `
      <div class=${styles["section__user-settings"]}>
         Settings Content
      </div>
   `;

   return markup;
};
