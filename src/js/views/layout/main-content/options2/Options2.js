import styles from "./_options2";

export const getOptions2Markup = () => {
   const markup = `
      <div class=${styles["section__options2"]}>
      Options 2 Content
      </div>
   `;

   return markup;
};
