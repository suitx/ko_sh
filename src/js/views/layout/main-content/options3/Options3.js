import styles from "./_options3";

export const getOptions3Markup = () => {
   const markup = `
      <div class=${styles["section__options3"]}>
      Options 3 Content
      </div>
   `;

   return markup;
};
