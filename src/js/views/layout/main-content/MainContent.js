import { routes } from "@routes/mainRoutes";
import { getEditableProfileMarkup } from "./editable-profile/EditableProfile";
import { getUserSettingsMarkup } from "./user-settings/UserSettings";
import { getOptions1Markup } from "./options1/Options1";
import { getOptions2Markup } from "./options2/Options2";
import { getOptions3Markup } from "./options3/Options3";

import styles from "./_main-content";
import u_styles from "@sass/base/_typography";

export const getMainContentMarkup = ({
   firstName,
   lastName,
   website,
   phone,
   address,
   currentRoute,
   currentPopupTypeInState,
   popupIsOpen,
   isInMobilePhoneMode,
   shouldShowEditForm,
}) => {
   let selectedRenderer;

   switch (currentRoute) {
      case routes[0]: {
         // About
         selectedRenderer = () =>
            getEditableProfileMarkup({
               firstName,
               lastName,
               website,
               phone,
               address,
               currentRoute,
               currentPopupTypeInState,
               popupIsOpen,
               isInMobilePhoneMode,
               shouldShowEditForm,
            });

         break;
      }

      case routes[1]: {
         // Settings
         selectedRenderer = getUserSettingsMarkup;
         break;
      }

      case routes[2]: {
         // Options 1
         selectedRenderer = getOptions1Markup;
         break;
      }

      case routes[3]: {
         // Options 2
         selectedRenderer = getOptions2Markup;
         break;
      }

      case routes[4]: {
         // Options 3
         selectedRenderer = getOptions3Markup;
         break;
      }

      default: {
         selectedRenderer = () =>
            getEditableProfileMarkup({
               firstName,
               lastName,
               website,
               phone,
               address,
               currentRoute,
               currentPopupTypeInState,
               popupIsOpen,
               isInMobilePhoneMode,
               shouldShowEditForm,
            });

         break;
      }
   }

   const markup = `
      <main class=${styles["main-content"]}>
         <section class=${styles["main-content__section"]}>
               <h2 class="${u_styles["heading"]} ${
      u_styles["heading--primary"]
   }">${currentRoute}</h2>
            ${selectedRenderer()}
         </section>
      </main>
   `;
   return markup;
};
