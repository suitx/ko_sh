import { getEditButtonMarkup } from "@components/buttons/EditButton/EditButton";
import { getPopupMarkup } from "@components/Popup/Popup";
import { getEditFormMarkup } from "@components/EditForm/EditForm";
import { getMobileEditFormMarkup } from "@components/MobileEditForm/MobileEditForm";

import { NAME, FIRST_NAME, LAST_NAME, WEB, ADDRESS, PHONE } from "@types/popup/view/types";
import { MOBILE } from "@types/button/actions/types";
import {
   YOUR_FIRST_NAME,
   YOUR_LAST_NAME,
   YOUR_WEBSITE_ADDRESS,
   YOUR_PHONE_NUMBER,
   CITY_STATE_ZIP,
} from "@localization/titles/titles";

import styles from "./_editableProfile";

export const getEditableProfileMarkup = ({
   firstName,
   lastName,
   website,
   phone,
   address,
   popupIsOpen,
   currentPopupTypeInState,
   isInMobilePhoneMode,
   shouldShowEditForm,
}) => {
   const restProfileFields = [
      { currentPopupType: WEB, title: YOUR_WEBSITE_ADDRESS, icon: "earth-outline", text: website },
      { currentPopupType: PHONE, title: YOUR_PHONE_NUMBER, icon: "call-outline", text: phone },
      { currentPopupType: ADDRESS, title: CITY_STATE_ZIP, icon: "home-outline", text: address },
   ];

   const nameFields = [
      { currentPopupType: FIRST_NAME, title: YOUR_FIRST_NAME, text: firstName },
      { currentPopupType: LAST_NAME, title: YOUR_LAST_NAME, text: lastName },
   ];

   debugger;

   // DESKTOP
   const desktopMarkup = `
   <div class=${styles["section__container"]}>
      <div class=${styles["section__row"]}>
            <div class=${styles["section__name"]}>${firstName} ${lastName}</div>
               ${getEditButtonMarkup({ currentButtonType: NAME })}
               ${getPopupMarkup({
                  isOpen: popupIsOpen,
                  currentPopupType: NAME,
                  currentPopupTypeInState,
                  renderChildren: () => getEditFormMarkup(nameFields),
               })}
            </div>

         ${restProfileFields
            .map(
               ({ currentPopupType, title, icon, text }) => `
         <div class=${styles["section__row"]}>
               <span class=${styles["section__icon"]}>
                  <ion-icon name=${icon}></ion-icon>
               </span>
               <div class=${styles["section__title"]}>${text}</div>
                  ${getEditButtonMarkup({ currentButtonType: currentPopupType })}
                  ${getPopupMarkup({
                     title,
                     isOpen: popupIsOpen,
                     currentPopupTypeInState,
                     currentPopupType,
                     renderChildren: () =>
                        getEditFormMarkup([{ currentPopupType, title, icon, text }]),
                  })}
         </div>
         `
            )
            .join("")}
      </div>`;

   // MOBILE
   const mobileMarkup = `
      <div class=${styles["section__container"]}>
         ${
            !shouldShowEditForm
               ? `
               <div class=${styles["section__mobile-button-container"]}>
                     ${getEditButtonMarkup({ currentButtonType: MOBILE })}
               </div>`
               : ""
         }
         ${
            shouldShowEditForm
               ? getMobileEditFormMarkup([...nameFields, ...restProfileFields])
               : `
            <div class=${styles["section__row"]}>
               <div class=${styles["section__name"]}>${firstName} ${lastName}</div>
            </div>
            ${restProfileFields
               .map(
                  ({ icon, text }) => `
                  <div class=${styles["section__row"]}>
                     <span class=${styles["section__icon"]}>
                        <ion-icon name=${icon}></ion-icon>
                     </span>
                  <div class=${styles["section__title"]}>${text}</div>
            </div>`
               )
               .join("")}`
         }
         </div>
      `;

   return isInMobilePhoneMode ? mobileMarkup : desktopMarkup;
};
