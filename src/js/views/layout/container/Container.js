import { getHeaderMarkup } from "../header/Header";
import { getMainContentMarkup } from "../main-content/MainContent";

import styles from "./_container";

export const getContainerMarkup = (props) => {
   debugger;
   const markup = `
      <div class="${styles.container}">
         ${getHeaderMarkup(props)}${getMainContentMarkup(props)}
      </div>
   `;

   return markup;
};
