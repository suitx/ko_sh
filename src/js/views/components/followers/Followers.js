import { getFollowButtonMarkup } from "@components/buttons/FollowButton/FollowButton";
import { FOLLOWERS } from "@localization/titles/titles";

import styles from "./_followers";

export const getFollowersMarkup = ({ followers }) => {
   const markup = `
      <div class=${styles["followers__container"]}>
         <div class=${styles["followers__inner-container"]}>
            ${getFollowButtonMarkup()}
            <div class=${styles["followers__text"]}>
               <span class=${styles["followers__number"]}>${followers}</span>
               <span class=${styles["followers__title"]}>${FOLLOWERS}</span>
            </div>
         </div>
      </div>
   `;

   return markup;
};
