import { routes } from "@routes/mainRoutes";

import styles from "./_tab-navigation";

export const getTabNavigationMarkup = ({ currentRoute }) => {
   const markup = `
      <nav class=${styles["tab-navigation__container"]}>
         <ul class=${styles["tab-navigation__ul"]}>
         ${routes
            .map(
               (page) => `
         <li class=${styles["tab-navigation__li"]}>
            <button type="button" data-route="${page}" class=${
                  currentRoute === page
                     ? styles["tab-navigation__nav-button--active"]
                     : styles["tab-navigation__nav-button"]
               }>
                ${page}
            </button>
        </li>`
            )
            .join("")}
         </ul>
      </nav>
   `;

   return markup;
};
