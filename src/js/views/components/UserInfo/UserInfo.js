import styles from "./_user-info";

export const getUserInfoMarkup = ({ firstName, lastName, address, phone, isInMobilePhoneMode }) => {
   const desktopMarkup = `
        <div class=${styles["userinfo-container__item"]}>
            <span class=${styles["userinfo-container__name"]}>${firstName} ${lastName}</span>
        </div>
        <div class=${styles["userinfo-container__item"]}>
            <div class=${styles["userinfo-container__row"]}>
                <span class=${styles["userinfo-container__icon"]}><ion-icon name="location-outline"></ion-icon></span>
                <div class=${styles["userinfo-container__title"]}>${address}</div>
            </div>
        </div>
        <div class=${styles["userinfo-container__item"]}>
            <div class=${styles["userinfo-container__row"]}>
                <span class=${styles["userinfo-container__icon"]}><ion-icon name="call-outline"></ion-icon></span>
                <div class=${styles["userinfo-container__title"]}>${phone}</div>
            </div>
        </div>
   `;

   const mobileMarkup = `
   <div class=${styles["userinfo-container__item"]}>
       <span class=${styles["userinfo-container__name"]}>${firstName} ${lastName}</span>
   </div>
   <div class=${styles["userinfo-container__item"]}>
        <div class=${styles["userinfo-container__row"]}>
            <span class=${styles["userinfo-container__icon"]}><ion-icon name="call-outline"></ion-icon></span>
            <div class=${styles["userinfo-container__title"]}>${phone}</div>
        </div>
    </div>
   <div class=${styles["userinfo-container__item"]}>
       <div class=${styles["userinfo-container__row"]}>
           <span class=${styles["userinfo-container__icon"]}><ion-icon name="location-outline"></ion-icon></span>
           <div class=${styles["userinfo-container__title"]}>${address}</div>
       </div>
   </div>
`;

   return isInMobilePhoneMode ? mobileMarkup : desktopMarkup;
};
