import { REVIEWERS } from "@localization/titles/titles";

import styles from "./_stars";

export const getStarsMarkup = ({ currentRating, reviewers }) => {
   const markup = `
      <div class=${styles["usersrating__container"]}>
            <div class=${styles["usersrating__row"]}>
            ${["☆", "☆", "☆", "☆", "☆"]
               .map((star, index) => {
                  const currentStarValue = index + 1;
                  const isStarInRating = currentStarValue <= currentRating && currentRating !== 0;

                  return `<span class=${
                     isStarInRating
                        ? styles["usersrating__star--selected"]
                        : styles["usersrating__star"]
                  } data-rating=${currentStarValue}>${star}</span>`;
               })
               .reverse()
               .join(" ")}
            </div>
            <div class=${styles["usersrating__review"]}>${reviewers}<span class=${
      styles["usersrating__reviewers"]
   }>${REVIEWERS}</span></div>
      </div>
   `;

   return markup;
};
