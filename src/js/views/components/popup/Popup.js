import styles from "./_popup";

export const getPopupMarkup = ({
   isOpen,
   renderChildren,
   currentPopupTypeInState,
   currentPopupType,
}) => {
   const markup = `
   <div class=${
      isOpen && currentPopupTypeInState === currentPopupType
         ? styles["popup__container--open"]
         : styles["popup__container--closed"]
   }>
      <div class=${styles["popup__container--tooltip-arrow"]}></div>
      ${renderChildren()}
   </div>
   `;
   return markup;
};
