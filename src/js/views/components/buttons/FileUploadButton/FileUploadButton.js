import { UPLOAD_COVER_IMAGE } from "@localization/labels/labels";

import styles from "./_fileuploadButton";

export const getFileuploadButtonMarkup = () => {
   const markup = `
   <div class=${styles["fileupload-button__container"]}>
        <label for="cover-image" class=${styles["fileupload-button__label"]}>
            <span class=${styles["fileupload-button__icon"]}>
               <ion-icon name="camera"></ion-icon>
            </span>
            ${UPLOAD_COVER_IMAGE}
         </label>
        <input type="file" id="cover-image" name="cover-image" class=${styles["fileupload-button__input"]}>
    </div>`;

   return markup;
};
