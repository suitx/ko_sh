import styles from "./_standardButton";

export const getStandardButtonMarkup = ({ label, actionType = "", buttonClass } = {}) => {
   const markup = `
      <button type="button" class=${
         buttonClass ? buttonClass : styles.button
      } data-actionType=${actionType}>
         ${label}
      </button>
   `;
   return markup;
};
