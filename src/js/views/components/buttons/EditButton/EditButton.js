import { MOBILE } from "@types/button/actions/types";

import styles from "./_editButton";

export const getEditButtonMarkup = ({ currentButtonType = "" } = {}) => {
   const markup =
      currentButtonType === MOBILE
         ? `
      <button 
         type="button" 
         class=${styles["edit-button__button--mobile"]} 
      >
            <ion-icon name="pencil-outline"></ion-icon>
      </button>
   `
         : `
   <button 
      type="button" 
      class=${styles["edit-button__button"]} 
      data-popupType=${currentButtonType}>
         <ion-icon 
            name="pencil-outline" 
            data-popupType=${currentButtonType}>
         </ion-icon>
   </button>
`;
   return markup;
};
