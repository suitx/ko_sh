import styles from "./_followButton";

export const getFollowButtonMarkup = () => {
   const markup = `
   <div class=${styles["follow-button__container"]}>
        <div class=${styles["follow-button__element"]}><ion-icon name="add-outline"></ion-icon></div>
    </div>`;

   return markup;
};
