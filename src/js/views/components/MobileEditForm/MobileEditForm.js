import { getStandardButtonMarkup } from "@components/buttons/StandardButton/StandardButton";

import { SAVE, CANCEL } from "@types/button/actions/types";
import { PHONE } from "@types/popup/view/types";
import { SAVE as SAVE_LABEL, CANCEL as CANCEL_LABEL } from "@localization/labels/labels";

import formStyles from "../EditForm/_editForm";
import standardButtonStyles from "@components/buttons/StandardButton/_standardButton";

export const getMobileEditFormMarkup = (fields) => {
   const markup = `
<form class=${formStyles["edit-form__container--mobile"]}>
   <div class=${formStyles["edit-form__buttons-container--mobile"]}>
      ${getStandardButtonMarkup({
         label: CANCEL_LABEL,
         actionType: CANCEL,
         buttonClass: standardButtonStyles["button--save-form"],
      })}
      ${getStandardButtonMarkup({
         label: SAVE_LABEL,
         actionType: SAVE,
         buttonClass: standardButtonStyles["button--save-form"],
      })}
   </div>

      ${fields
         .map(
            ({ currentPopupType, title, text }) => `
            <div class=${formStyles["edit-form__inner-container--mobile"]}>
                  <div class=${formStyles["edit-form__title"]}>${title}</div>
                  <input 
                     type=${currentPopupType === PHONE ? "tel" : "text"} 
                     name="${title}"
                     class=${formStyles["edit-form__input"]} 
                     data-input-type=${currentPopupType}
                     value=${text}
                     required
                  >
                  <span class=${formStyles["edit-form__highlight"]}></span>
                  <span class=${formStyles["edit-form__bar"]}></span>
                  <label class=${formStyles["edit-form__label"]}>${text}</label>
            </div>
      `
         )
         .join(" ")}
</form>
   `;
   debugger;
   return markup;
};
