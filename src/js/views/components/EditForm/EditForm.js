import { getStandardButtonMarkup } from "@components/buttons/StandardButton/StandardButton";

import { SAVE, CANCEL } from "@types/button/actions/types";
import { PHONE } from "@types/popup/view/types";
import { SAVE as SAVE_LABEL, CANCEL as CANCEL_LABEL } from "@localization/labels/labels";

import styles from "./_editForm";
import standardButtonStyles from "@components/buttons/StandardButton/_standardButton";

export const getEditFormMarkup = (fields) => {
   const markup = `
      <form class=${styles["edit-form__container"]}>
         ${fields
            .map(
               ({ currentPopupType, title, text }) => `
               <div class=${styles["edit-form__inner-container"]}>
                  <div class=${styles["edit-form__title"]}>${title}</div>
                     <input 
                        type=${currentPopupType === PHONE ? "tel" : "text"}
                        name="${title}"
                        class=${styles["edit-form__input"]} 
                        data-input-type=${currentPopupType} 
                        value=${text}
                        required
                     >
                     <span class=${styles["edit-form__highlight"]}></span>
                     <span class=${styles["edit-form__bar"]}></span>
                     <label class=${styles["edit-form__label"]}>${text}</label>
               </div>`
            )
            .join(" ")}

   <div class=${styles["edit-form__buttons-container"]}>
      ${getStandardButtonMarkup({
         label: SAVE_LABEL,
         actionType: SAVE,
         buttonClass: standardButtonStyles["button--save-form"],
      })}
      ${getStandardButtonMarkup({
         label: CANCEL_LABEL,
         actionType: CANCEL,
         buttonClass: standardButtonStyles["button--save-form"],
      })}
   </div>
</form>
   `;
   debugger;
   return markup;
};
