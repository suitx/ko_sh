import { getContainerMarkup } from "@layout/container/Container";

class View {
   render(props) {
      const root = document?.querySelector(".root");

      if (root) {
         // Rerender app, if it's instantiated already
         root.remove();

         const app = document.createElement("div");
         app.classList.add("root");

         app.insertAdjacentHTML("afterbegin", getContainerMarkup(props));
         document.body.appendChild(app);
      } else if (!root) {
         // Otherwise create the first app instance
         const app = document.createElement("div");
         app.classList.add("root");

         app.insertAdjacentHTML("afterbegin", getContainerMarkup(props));
         document.body.appendChild(app);
      }
   }
}

export default View;
