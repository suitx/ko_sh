import UserModel from "@models/User";
import View from "@views/defaultView";
import Router from "@router/router";

import { SAVE, CANCEL } from "@types/button/actions/types";
import { NAME, FIRST_NAME, LAST_NAME, PHONE, ADDRESS, WEB } from "@types/popup/view/types";

import navigationClassnames from "@views/components/TabNavigation/_tab-navigation";
import followButtonClassnames from "@views/components/buttons/FollowButton/_followButton";
import ratingStarClassnames from "@views/components/Stars/_stars";
import editProfileClasses from "@views/layout/main-content/editable-profile/_editableProfile";
import editFormClasses from "@views/components/EditForm/_editForm";
import editButtonClasses from "@views/components/buttons/EditButton/_editButton";
import standardButtonClasses from "@views/components/buttons/StandardButton/_standardButton";

class Controller {
   constructor() {
      this.model = new UserModel();
      this.view = new View();
      this.router = new Router();
   }

   // TO DO: make rerendering method for slices of the state

   /* User's data */
   handleEditFirstName = ({ firstName }) => {
      this.model.editFirstName({ firstName });
   };

   handleEditLastName = ({ lastName }) => {
      this.model.editLastName({ lastName });
   };

   handleEditWebsite = ({ website }) => {
      this.model.editWebsite({ website });
   };

   handleEditPhone = ({ phone }) => {
      this.model.editPhone({ phone });
   };

   handleEditAddress = ({ address }) => {
      this.model.editAddress({ address });
   };

   handleEditRating = ({ rating }) => {
      this.model.editRating({ rating });
   };

   handleEditReviewers = ({ reviewers }) => {
      this.model.editReviewers({ reviewers });
   };

   handleEditFollowers = ({ followers }) => {
      this.model.editFollowers({ followers });
   };
   /* *** */

   /* Popup's open/closed state */
   handleClosePopup = () => {
      this.model.closePopup();
   };

   handleOpenPopup = ({ selectedPopupType }) => {
      this.model.openPopup({ selectedPopupType });
   };
   /* *** */

   /* Selected html elements */
   saveRefToOpenedPopupForm = ({ selectedElement }) => {
      this.model.setOpenedPopupForm({ selectedElement });
   };

   saveRefToCurrentInputValue = ({ inputValue }) => {
      this.model.setCurrentInputValue({ inputValue });
   };
   /* *** */

   /* Routing state */
   handleChangeRoute = ({ newRoute }) => {
      this.router.changeRoute({ newRoute });
   };
   /* *** */

   /* Current user client (mobile/desktop) */
   setCurrentUserClient = () => {
      this.model.setCurrentUserClient();
   };
   /* *** */

   /* Mobile edit form open/closed state */
   handleCloseMobileEditForm = () => {
      this.model.closeMobileEditForm();
   };

   handleOpenMobileEditForm = () => {
      this.model.openMobileEditForm();
   };
   /* *** */

   run = () => {
      debugger;
      this.view.render({
         firstName: this.model.profile.firstName,
         lastName: this.model.profile.lastName,
         website: this.model.profile.website,
         phone: this.model.profile.phone,
         address: this.model.profile.address,
         currentRating: this.model.profile.currentRating,
         reviewers: this.model.profile.reviewers,
         followers: this.model.profile.followers,
         currentRoute: this.router.currentRoute,
         currentPopupTypeInState: this.model.profile.currentPopupTypeInState,
         popupIsOpen: this.model.profile.popupIsOpen,
         isInMobilePhoneMode: this.model.profile.isInMobilePhoneMode,
         shouldShowEditForm: this.model.profile.shouldShowEditForm,
      });

      /* Check what current user client is (mobile/desktop) */
      this.setCurrentUserClient();
      /* *** */

      /* Event handlers */
      // Routing
      const addRouteChangeListener = () => {
         const navigationTabElement = document.querySelector(
            `.${navigationClassnames["tab-navigation__container"]}`
         );

         const routeHandler = (event) => {
            const selectedRoute = event.target.dataset.route;

            if (selectedRoute && selectedRoute !== this.router.currentRoute) {
               this.handleChangeRoute({ newRoute: selectedRoute });
               this.run();
            }
         };

         if (navigationTabElement?.addEventListener) {
            navigationTabElement.addEventListener("click", routeHandler);
         } else if (navigationTabElement?.attachEvent) {
            navigationTabElement.attachEvent("onclick", routeHandler);
         }
      };

      // Data editing
      const addFollowersChangeListener = () => {
         const followButtonElement = document.querySelector(
            `.${followButtonClassnames["follow-button__element"]}`
         );

         const followersHandler = () => {
            this.handleEditFollowers({ followers: 1 });
            this.run();
         };

         if (followButtonElement?.addEventListener) {
            followButtonElement.addEventListener("click", followersHandler);
         } else if (followButtonElement?.attachEvent) {
            followButtonElement.attachEvent("onclick", followersHandler);
         }
      };

      const addRatingChangeListener = () => {
         const ratingStarElement = document.querySelector(
            `.${ratingStarClassnames["usersrating__container"]}`
         );

         const ratingHandler = (event) => {
            const selectedRating = event.target.dataset.rating;

            if (selectedRating) {
               this.handleEditRating({ rating: selectedRating });
               this.handleEditReviewers({ reviewers: 1 });
               this.run();
            }
         };

         if (ratingStarElement?.addEventListener) {
            ratingStarElement.addEventListener("click", ratingHandler);
         } else if (ratingStarElement?.attachEvent) {
            ratingStarElement.attachEvent("onclick", ratingHandler);
         }
      };

      // Managing open/close state of the popup-form
      const addPopupStateListener = () => {
         if (!this.model.profile.isInMobilePhoneMode) {
            const editProfileContainerElements = [
               ...document.querySelectorAll(`.${editButtonClasses["edit-button__button"]}`),
            ];
            debugger;
            const popupStateHandler = (event) => {
               // Handle open/close of edit popup
               debugger;
               if (
                  event.target.className === standardButtonClasses["button--save-form"] ||
                  (event.target.className === "md hydrated" &&
                     !event.target.parentElement.className.includes("button--mobile"))
               ) {
                  if (this.model.profile.popupIsOpen === false) {
                     this.handleOpenPopup({ selectedPopupType: event.target.dataset.popuptype });
                     this.run();
                  } else if (this.model.profile.popupIsOpen === true) {
                     this.handleClosePopup();
                     this.run();
                  }
               }
            };

            if (
               editProfileContainerElements.length &&
               editProfileContainerElements[0].addEventListener
            ) {
               editProfileContainerElements.forEach((editProfileContainerElement) => {
                  editProfileContainerElement.addEventListener("click", popupStateHandler);
               });
            } else if (
               editProfileContainerElements.length &&
               editProfileContainerElements[0].attachEvent
            ) {
               editProfileContainerElements.forEach((editProfileContainerElement) => {
                  editProfileContainerElement.attachEvent("onclick", popupStateHandler);
               });
            }
         }
      };

      // Managing open/close state of the mobile edit-form
      const addMobileEditFormStateListener = () => {
         if (this.model.profile.isInMobilePhoneMode) {
            const editProfileContainerElement = document.querySelector(
               `.${editProfileClasses["section__container"]}`
            );
            debugger;
            const popupStateHandler = (event) => {
               // Handle open/close of the mobile edit form
               debugger;
               if (
                  event.target.className === editButtonClasses["edit-button__button--mobile"] ||
                  (event.target.className === "md hydrated" &&
                     event.target.parentElement.className.includes("button--mobile"))
               ) {
                  if (this.model.profile.shouldShowEditForm === false) {
                     this.handleOpenMobileEditForm();
                     this.run();
                  } else if (this.model.profile.shouldShowEditForm === true) {
                     this.handleCloseMobileEditForm();
                     this.run();
                  }
               }
            };

            if (editProfileContainerElement?.addEventListener) {
               editProfileContainerElement.addEventListener("click", popupStateHandler);
            } else if (editProfileContainerElement?.attachEvent) {
               editProfileContainerElement.attachEvent("onclick", popupStateHandler);
            }
         }
      };

      // Managing popup-form values
      const addPopupFormStateListener = () => {
         if (!this.model.profile.isInMobilePhoneMode) {
            const currentPopupFormElement = document.querySelector(
               `.${editProfileClasses["section__container"]}`
            );
            debugger;

            const popupFormStateHandler = (event) => {
               // Handle save/cancel editing result in the popup form
               debugger;
               if (event.target.className === standardButtonClasses["button--save-form"]) {
                  const actionType = event.target.dataset.actiontype;

                  if (actionType === SAVE) {
                     switch (this.model.profile.currentPopupTypeInState) {
                        case NAME: {
                           const firstNameInputElement = document.querySelector(
                              `[data-input-type=${FIRST_NAME}]`
                           );
                           const lastNameInputElement = document.querySelector(
                              `[data-input-type=${LAST_NAME}]`
                           );

                           const firstNameFormFieldValue = firstNameInputElement.value;
                           const lastNameFormFieldValue = lastNameInputElement.value;

                           this.handleEditFirstName({ firstName: firstNameFormFieldValue });
                           this.handleEditLastName({ lastName: lastNameFormFieldValue });
                           this.handleClosePopup();
                           this.run();

                           break;
                        }

                        case PHONE: {
                           const currentInputElement = document.querySelector(
                              `[data-input-type=${PHONE}]`
                           );
                           const formFieldValue = currentInputElement.value;

                           this.handleEditPhone({ phone: formFieldValue });
                           this.handleClosePopup();
                           this.run();

                           break;
                        }

                        case ADDRESS: {
                           const currentInputElement = document.querySelector(
                              `[data-input-type=${ADDRESS}]`
                           );
                           const formFieldValue = currentInputElement.value;

                           this.handleEditAddress({ address: formFieldValue });
                           this.handleClosePopup();
                           this.run();

                           break;
                        }

                        case WEB: {
                           const currentInputElement = document.querySelector(
                              `[data-input-type=${WEB}]`
                           );
                           const formFieldValue = currentInputElement.value;

                           this.handleEditWebsite({ website: formFieldValue });
                           this.handleClosePopup();
                           this.run();

                           break;
                        }

                        default: {
                           console.error("No such type of action for this field");
                        }
                     }
                  } else if (actionType === CANCEL) {
                     this.handleClosePopup();
                     this.run();
                  }
               }
            };

            if (currentPopupFormElement?.addEventListener) {
               currentPopupFormElement.addEventListener("click", popupFormStateHandler);
               currentPopupFormElement.addEventListener("submit", (event) =>
                  event.preventDefault ? event.preventDefault() : (event.returnValue = false)
               );
            } else if (currentPopupFormElement?.attachEvent) {
               currentPopupFormElement.attachEvent("onclick", (event) =>
                  event.preventDefault ? event.preventDefault() : (event.returnValue = false)
               );
            }
         }
      };

      // Managing mobile form values
      const addMobileFormValuesListener = () => {
         if (this.model.profile.isInMobilePhoneMode) {
            const currentMobileFormElement = document.querySelector(
               `.${editFormClasses["edit-form__container--mobile"]}`
            );
            debugger;

            const mobileFormValuesHandler = (event) => {
               // Handle save/cancel editing result in the mobile form
               debugger;
               if (event.target.className === standardButtonClasses["button--save-form"]) {
                  const actionType = event.target.dataset.actiontype;

                  if (actionType === SAVE) {
                     const firstNameInputElement = document.querySelector(
                        `[data-input-type=${FIRST_NAME}]`
                     );
                     const lastNameInputElement = document.querySelector(
                        `[data-input-type=${LAST_NAME}]`
                     );
                     const phoneInputElement = document.querySelector(`[data-input-type=${PHONE}]`);
                     const addressInputElement = document.querySelector(
                        `[data-input-type=${ADDRESS}]`
                     );
                     const webInputElement = document.querySelector(`[data-input-type=${WEB}]`);

                     const firstNameFormFieldValue = firstNameInputElement.value;
                     const lastNameFormFieldValue = lastNameInputElement.value;
                     const phoneFieldValue = phoneInputElement.value;
                     const addressFieldValue = addressInputElement.value;
                     const webFieldValue = webInputElement.value;

                     this.handleEditFirstName({ firstName: firstNameFormFieldValue });
                     this.handleEditLastName({ lastName: lastNameFormFieldValue });
                     this.handleEditPhone({ phone: phoneFieldValue });
                     this.handleEditAddress({ address: addressFieldValue });
                     this.handleEditWebsite({ website: webFieldValue });

                     this.handleCloseMobileEditForm();
                     this.run();
                  } else if (actionType === CANCEL) {
                     this.handleCloseMobileEditForm();
                     this.run();
                  }
               }
            };

            if (currentMobileFormElement?.addEventListener) {
               currentMobileFormElement.addEventListener("click", mobileFormValuesHandler);
               currentMobileFormElement.addEventListener("submit", (event) =>
                  event.preventDefault ? event.preventDefault() : (event.returnValue = false)
               );
            } else if (currentMobileFormElement?.attachEvent) {
               currentMobileFormElement.attachEvent("onclick", (event) =>
                  event.preventDefault ? event.preventDefault() : (event.returnValue = false)
               );
            }
         }
      };
      /* *** */

      addRouteChangeListener();
      addFollowersChangeListener();
      addRatingChangeListener();
      addPopupStateListener();
      addPopupFormStateListener();
      addMobileEditFormStateListener();
      addMobileFormValuesListener();
      /* *** */
   };
}

export default Controller;
