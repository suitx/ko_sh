import { NAME } from "@types/popup/view/types";

class User {
   constructor() {
      const commitedState = JSON.parse(localStorage.getItem("user_profile"));
      if (commitedState) {
         this.profile = commitedState;
      } else {
         this.profile = {
            firstName: "Jessica",
            lastName: "Parker",
            website: "www.seller.com",
            phone: "(949) 325 - 69594",
            address: "Newport Beach, CA",
            ratings: [],
            currentRating: 1,
            reviewers: 0,
            followers: 15,

            // TO DO: Separate non visual data for view
            currentPopupTypeInState: NAME,
            popupIsOpen: false,

            openedPopupForm: "", // Classname of the element
            currentInputValue: "",

            isInMobilePhoneMode: window.screen.width <= 600,
            shouldShowEditForm: false,
         };

         localStorage.setItem("user_profile", JSON.stringify(this.profile));
      }
   }

   _commitToStorage(profile) {
      localStorage.setItem("user_profile", JSON.stringify(profile));
   }

   editFirstName({ firstName }) {
      debugger;
      this.profile = { ...this.profile, firstName: firstName };
      this._commitToStorage(this.profile);
   }

   editLastName({ lastName }) {
      debugger;
      this.profile = { ...this.profile, lastName: lastName };
      this._commitToStorage(this.profile);
   }

   editWebsite({ website }) {
      this.profile = { ...this.profile, website: website };
      this._commitToStorage(this.profile);
   }

   editPhone({ phone }) {
      this.profile = { ...this.profile, phone: phone };
      this._commitToStorage(this.profile);
   }

   editAddress({ address }) {
      this.profile = { ...this.profile, address: address };
      this._commitToStorage(this.profile);
   }

   editRating({ rating }) {
      // count as overall mean rating
      try {
         if (this.profile.ratings.length > 0) {
            // Some ratings already exist
            const sumOfCurrentRatings = parseInt(
               this.profile.ratings.reduce(
                  (prev, current) => parseInt(prev, 10) + parseInt(current, 10)
               ),
               10
            );

            const newRating = Math.ceil(sumOfCurrentRatings / this.profile.ratings.length);

            this.profile = {
               ...this.profile,
               ratings: [...this.profile.ratings, rating],
               currentRating: newRating,
            };
            this._commitToStorage(this.profile);
         } else {
            // There are no ratings yet
            this.profile = {
               ...this.profile,
               ratings: [...this.profile.ratings, rating],
               currentRating: rating,
            };
            this._commitToStorage(this.profile);
         }
      } catch (error) {
         console.error(error.message);
      }
   }

   editReviewers({ reviewers }) {
      // sumarize all reviewers
      this.profile = { ...this.profile, reviewers: this.profile.reviewers + reviewers };
      this._commitToStorage(this.profile);
   }

   editFollowers({ followers }) {
      // sumarize all followers
      this.profile = { ...this.profile, followers: this.profile.followers + followers };
      this._commitToStorage(this.profile);
   }

   /* Html elements */
   setOpenedPopupForm = ({ selectedElement }) => {
      this.profile = { ...this.profile, openedPopupForm: selectedElement };
      this._commitToStorage(this.profile);
   };

   setCurrentInputValue = ({ inputValue }) => {
      debugger;
      this.profile = { ...this.profile, currentInputValue: inputValue };
      this._commitToStorage(this.profile);
   };

   openMobileEditForm = () => {
      debugger;
      this.profile = { ...this.profile, shouldShowEditForm: true };
      this._commitToStorage(this.profile);
   };

   closeMobileEditForm = () => {
      debugger;
      this.profile = { ...this.profile, shouldShowEditForm: false };
      this._commitToStorage(this.profile);
   };
   /* *** */

   /* Popup dialogs state (TO DO: divide into own group) */
   closePopup() {
      this.profile = { ...this.profile, popupIsOpen: false };
      this._commitToStorage(this.profile);
   }

   openPopup({ selectedPopupType }) {
      this.profile = {
         ...this.profile,
         popupIsOpen: true,
         currentPopupTypeInState: selectedPopupType,
      };
      this._commitToStorage(this.profile);
   }
   /* *** */

   /* Mobile or desktop */
   setCurrentUserClient() {
      this.profile = { ...this.profile, isInMobilePhoneMode: window.screen.width <= 600 };
      this._commitToStorage(this.profile);
   }
   /* *** */
}

export default User;
