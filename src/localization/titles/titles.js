export const FOLLOWERS = "Followers";
export const REVIEWERS = "Reviews";
export const YOUR_FIRST_NAME = "Your first name";
export const YOUR_LAST_NAME = "Your last name";
export const YOUR_WEBSITE_ADDRESS = "Your website address";
export const YOUR_PHONE_NUMBER = "Your phone number";
export const CITY_STATE_ZIP = "City, state & zip";
